require 'net/https'
require 'uri'

module CaWage
  class ApiClient
    def school_years
      school_years = []
      years = [2013, 2012, 2011, 2010]
      schools = ['Berkeley', 'Davis', 'Irvine', 'Los Angeles', 'Merced', 'Riverside', 'San Diego', 'San Francisco',
      'Santa Barbara', 'Santa Cruz', 'UCOP']

      years.each do |year|
        schools.each do |school|
          school_years << [school, year]
        end
      end
      
      school_years
    end

    def get_page_count(school_name, year)
      uri = URI.parse('https://ucannualwage.ucop.edu/wage/search.action')
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Post.new(uri.request_uri)
      request.set_form_data(
        _search: false,
        nd: 1380253091396,
        year: year,
        location: school_name,
        rows: 60,
        sidx: 'EAW_LST_NAM',
        sord: 'asc',
      )

      response = http.request(request)

      page_count = /\d+/.match(response.body.to_s.split(" ")[6])
      page_count.to_s.to_i
    end

    def get_data(school_name, year, page_count)
      uri = URI.parse('https://ucannualwage.ucop.edu/wage/search.action')
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Post.new(uri.request_uri)
      request.set_form_data(
        page: page_count,
        _search: false,
        nd: 1380253091396,
        year: year,
        location: school_name,
        rows: 60,
        sidx: 'EAW_LST_NAM',
        sord: 'asc',
      )

      response = http.request(request)

      response.body.gsub("'", "\"")
    end
  end
end
