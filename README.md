This program scrapes and downloads payroll data from the [University of California Employee Pay website](https://ucannualwage.ucop.edu/wage/). 

In previous years, Division of Agriculture and Natural Resources employees were reported under their own location ("DANR"). As of July 2014, they are reported under whichever campus or office their payroll is processed: Berkeley, Davis, Riverside, or the UC Office of the President.
(confirmed by UCOP comms 8/14/14)


## Running the code

To download data serially: 
$  ruby bin/extract ./data

To download data using threads:
$  ruby bin/extract ./data threads